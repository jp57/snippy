
//header file (.h) contains class definition


//Header guards to prevent multiple includes
#ifndef DEMO_H
#define DEMO_H

 class Demo
 {
    private:  //properties
         int var;
         double time;

    public:   //methods
        
         Demo();//default constructor (no parameters)
       Demo(int, double);// "overloaded" constructor

          void setVar(int);// set (change) the value of var
          int getVar();// view contents of variable var
          void setTime(double);
          double getTime();           

 };


#endif
