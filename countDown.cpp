
#include <iostream>
using namespace std;


void countdown(int n)
{
	if(n > 0)
	{
		cout<<n<<endl;
		countdown(n);
	}
	else
	   cout<<"Blastoff!!"<<endl;
}


int main()
{
	int x = 6;
	
	countdown(x);   
	
	return 0;
}
